import {Injectable} from '@angular/core';
import {CanActivate, Router, UrlTree} from '@angular/router';
import {UserSessionService} from './user-session.service';

@Injectable({
  providedIn: 'root'
})
export class UserSettingsGuard implements CanActivate {

  constructor(private userSessionService: UserSessionService, private router: Router) {
  }

  canActivate(): boolean | UrlTree {
    if (this.userSessionService.userIsLoggedIn()) {
      return true;
    } else {
      return this.router.parseUrl('/');
    }
  }
}
