import {AbstractControl, ValidatorFn} from '@angular/forms';

export function notPatternValidator(pattern: RegExp): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const forbidden = pattern.test(control.value);
    return forbidden ? {'not-pattern': {value: control.value}} : null;
  };
}
