import {Injectable} from '@angular/core';
import {InMemoryDbService, ResponseOptions} from 'angular-in-memory-web-api';
import * as contactArray from './initial-contacts.json';
import {Contact} from './contact';
import MyUtils from './my-utils';

@Injectable({
  providedIn: 'root'
})
export class InMemoryContactsService implements InMemoryDbService {

  createDb() {
    const contacts: Contact[] = [];

    if (MyUtils.hasLocalStorage() && localStorage.getItem('contacts') !== null) {
      JSON.parse(localStorage.getItem('contacts')).forEach((contact: Contact) => contacts.push(Contact.fromAny(contact)));
    } else {
      (contactArray as any).default.forEach((contact: Contact) => contacts.push(Contact.fromAny(contact)));
    }
    return {contacts};
  }

  get(reqInfo: any): ResponseOptions | undefined {
    if (reqInfo.collectionName === 'contacts' && reqInfo.id === undefined && MyUtils.hasLocalStorage()) {
      localStorage.setItem(reqInfo.collectionName, JSON.stringify(reqInfo.collection));
    }
    return undefined;
  }
}
