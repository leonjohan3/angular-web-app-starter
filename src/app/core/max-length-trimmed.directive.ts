import {AbstractControl, ValidatorFn} from '@angular/forms';

export function maxLengthTrimmedValidator(maxLength: number): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const forbidden = control.value.trim().length > 0 && control.value.trim().length > maxLength;
    return forbidden ? {maxlengthTrimmed: {value: control.value}} : null;
  };
}
