import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserSessionService {

  private attributes = new Map<string, string>();

  getAttributes(): Map<string, string> {
    return this.attributes;
  }

  resetAttributes(): void {
    this.attributes = new Map<string, string>();
  }

  userIsLoggedIn(): boolean {
    return this.attributes.size > 0;
  }

}
