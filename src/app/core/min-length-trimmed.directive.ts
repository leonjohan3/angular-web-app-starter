import {AbstractControl, ValidatorFn} from '@angular/forms';

export function minLengthTrimmedValidator(minLength: number): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    const forbidden = control.value.length > 0 && control.value.trim().length < minLength;
    return forbidden ? {minlengthTrimmed: {value: control.value}} : null;
  };
}
