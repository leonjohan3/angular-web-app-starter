import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError, map, tap} from 'rxjs/operators';
import {Contact} from './contact';


@Injectable({
  providedIn: 'root'
})
export class ContactService {
  private contactsUrl = 'api/contacts';  // URL to web api
  // private contactsSbUrl = 'sb/api/contacts';  // URL to Spring Boot App

  readonly httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  };

  constructor(private http: HttpClient) {
  }

  /** GET contacts from the server */
  getContacts(): Observable<Contact[]> {
    return this.http.get<Contact[]>(this.contactsUrl)
      .pipe(
        map(data => {
          const contacts: Contact[] = [];
          data.forEach(item => contacts.push(Contact.fromAny(item)));
          // console.log('map', contacts);
          return contacts;
        }),
        // tap(_ => console.log('fetched contacts', _)),
        catchError(this.handleError<Contact[]>('getContacts', []))
      );
  }

  // getSbContacts(): Observable<Contact[]> {
  //   return this.http.get<Contact[]>(this.contactsSbUrl)
  //     .pipe(
  //       map(data => {
  //         const contacts: Contact[] = [];
  //         data.forEach(item => contacts.push(Contact.fromAny(item)));
  //         // console.log('map', contacts);
  //         return contacts;
  //       }),
  //       // tap(_ => console.log('fetched sb contacts', _)),
  //       catchError(this.handleError<Contact[]>('getContacts', []))
  //     );
  // }

  getContact(id: number): Observable<Contact | null> {
    const url = `${this.contactsUrl}/?id=${id}`;
    return this.http.get<Contact[]>(url).pipe(
      map(contacts => contacts && contacts.length && contacts.length > 0 ? Contact.fromAny(contacts[0]) : null),
      tap(h => {
        // console.log('h', h);
        const outcome = h ? 'fetched' : 'did not find';
        console.log(`${outcome} contact id=${id}`);
      }),
      catchError(this.handleError<Contact>(`getContact id=${id}`)));
  }

  // getSbContact(id: number): Observable<Contact | null> {
  //   const url = `${this.contactsSbUrl}/?id=${id}`;
  //   return this.http.get<Contact[]>(url).pipe(
  //     map(contacts => contacts && contacts.length && contacts.length > 0 ? Contact.fromAny(contacts[0]) : null),
  //     tap(h => {
  //       // console.log('h', h);
  //       const outcome = h ? 'fetched' : 'did not find';
  //       console.log(`${outcome} contact id=${id}`);
  //     }),
  //     catchError(this.handleError<Contact>(`getContact id=${id}`)));
  // }

  updateContact(contact: Contact): Observable<any> {
    return this.http.put(this.contactsUrl, contact, this.httpOptions).pipe(
      tap(() => console.log(`updated Contact id=${contact._id}`)),
      catchError(this.handleError<any>('updateContact'))
    );
  }

  // updateSbContact(contact: Contact): Observable<any> {
  //   return this.http.put(this.contactsSbUrl, contact, this.httpOptions).pipe(
  //     tap(_ => console.log(`updated Contact id=${contact._id}`)),
  //     // tap(_ => console.log(`updated Contact: ${_}`)),
  //     catchError(this.handleError<any>('updateContact'))
  //   );
  // }

  addContact(contact: Contact): Observable<Contact> {
    return this.http.post<Contact>(this.contactsUrl, contact, this.httpOptions).pipe(
      tap((newContact: any) => console.log(`added Contact w/ id=${newContact.id}`)),
      catchError(this.handleError<Contact>('addContact'))
    );
  }

  // addSbContact(contact: Contact): Observable<Contact> {
  //   console.log('addContact', contact);
  //   return this.http.post<Contact>(this.contactsSbUrl, contact, this.httpOptions).pipe(
  //     tap((newContact: any) => console.log(`added Contact w/ id=${newContact.id}`, newContact)),
  //     catchError(this.handleError<Contact>('addContact'))
  //   );
  // }

  /** DELETE: delete the Contact from the server */
  deleteContact(contact: Contact | number): Observable<Contact> {
    const id = typeof contact === 'number' ? contact : contact._id;
    const url = `${this.contactsUrl}/${id}`;

    return this.http.delete<Contact>(url, this.httpOptions).pipe(
      tap(() => console.log(`deleted Contact id=${id}`)),
      catchError(this.handleError<Contact>('deleteContact'))
    );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error('handleError', error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
