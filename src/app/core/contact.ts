export class Contact {

  static readonly nameMaxLength = 80;
  static readonly nameMinLength = 2;
  static readonly emailMaxLength = 128;
  static readonly emailMinLength = 5;
  static readonly phoneNumberMaxLength = 32;
  static readonly phoneNumberMinLength = 5;
  static readonly phoneNumberRegExp = /^[ext:\d\-\s+()/.]{5,}$/;

  private readonly id: number | null;
  private firstName: string;
  private lastName: string;
  private email: string;
  private phoneNumber: string;
  private birthDate: Date;

  constructor(id: number | null, firstName: string, lastName: string, email: string, phoneNumber: string, birthDate: Date) {
    if (id && id <= 0) {
      throw new Error(`id must be greater than zero, id: ${id}`);
    }
    this.id = id;
    this._firstName = firstName;
    this._lastName = lastName;
    this._email = email;
    this._phoneNumber = phoneNumber;
    this._birthDate = new Date(birthDate.getTime());
  }

  static fromAny(value: any): Contact {
    const contact = new Contact(value.id, value.firstName, value.lastName, value.email, value.phoneNumber, new Date(value.birthDate));
    return contact;
  }

  clone(): Contact {
    return new Contact(this._id, this._firstName, this._lastName, this._email, this._phoneNumber, new Date(this._birthDate.getTime()));
  }

  equals(contact: Contact): boolean {
    return this._id === contact._id && this._firstName === contact._firstName && this._lastName === contact._lastName
      && this._email === contact._email && this._phoneNumber === contact._phoneNumber
      && this._birthDate.getTime() === contact._birthDate.getTime();
  }

  get _id(): number | null {
    return this.id;
  }

  get _firstName(): string {
    return this.firstName;
  }

  set _firstName(value: string) {
    value = value.trim();

    if (value.length > Contact.nameMaxLength) {
      throw new Error(`firstName has a max length of ${Contact.nameMaxLength}`);
    }
    if (value.length < Contact.nameMinLength) {
      throw new Error(`firstName has a minimum length of ${Contact.nameMinLength}`);
    }
    this.firstName = value;
  }

  get _lastName(): string {
    return this.lastName;
  }

  set _lastName(value: string) {
    value = value.trim();

    if (value.length > Contact.nameMaxLength) {
      throw new Error(`lastName has a max length of ${Contact.nameMaxLength}`);
    }
    if (value.length < Contact.nameMinLength) {
      throw new Error(`lastName has a minimum length of ${Contact.nameMinLength}`);
    }
    this.lastName = value;
  }

  get _email(): string {
    return this.email;
  }

  set _email(value: string) {
    value = value.trim();

    if (value.length > Contact.emailMaxLength) {
      throw new Error(`email has a max length of ${Contact.emailMaxLength}`);
    }
    if (value.length < Contact.emailMinLength) {
      throw new Error(`email has a minimum length of ${Contact.emailMinLength}`);
    }
    const pattern = new RegExp(/^[^@]+@[^@]+$/);

    if (!pattern.test(value)) {
      throw new Error(`invalid email: ${value}`);
    }
    this.email = value;
  }

  get _phoneNumber(): string {
    return this.phoneNumber;
  }

  set _phoneNumber(value: string) {
    value = value.trim();

    if (value.length > Contact.phoneNumberMaxLength) {
      throw new Error(`phoneNumber has a max length of ${Contact.phoneNumberMaxLength}`);
    }
    if (value.length < Contact.phoneNumberMinLength) {
      throw new Error(`phoneNumber has a minimum length of ${Contact.phoneNumberMinLength}`);
    }
    const pattern = new RegExp(Contact.phoneNumberRegExp);

    if (!pattern.test(value)) {
      throw new Error(`invalid phoneNumber: ${value}`);
    }
    this.phoneNumber = value;
  }

  get _birthDate(): Date {
    return this.birthDate;
  }

  set _birthDate(value: Date) {
    if (value.getTime() > Date.now()) {
      throw new Error(`invalid birth date: ${value.toLocaleDateString()}, must not be in the future`);
    }
    this.birthDate = value;
  }

}
