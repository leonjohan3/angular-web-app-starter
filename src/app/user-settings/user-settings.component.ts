import {Component} from '@angular/core';
import {UserSessionService} from '../core/user-session.service';

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent {
  displayedColumns = ['attribute', 'value'];
  tableData: Row[] = [];
  pictureSrc: string;

  constructor(private userSessionService: UserSessionService) {

    this.userSessionService.getAttributes().forEach((value, key) => {
      if (key === 'picture') {
        this.pictureSrc = value;
      } else {
        if (/iat|exp/.test(key)) {
          this.tableData.push({attribute: key, value: new Date(+value * 1000)});
        } else {
          this.tableData.push({attribute: key, value});
        }
      }
    });
  }
}

interface Row {
  attribute: string;
  value?: string | Date;
}
