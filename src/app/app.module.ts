import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {FlexLayoutModule} from '@angular/flex-layout';

import {MatSliderModule} from '@angular/material/slider';
import {LayoutModule} from '@angular/cdk/layout';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card';
import {MatMenuModule} from '@angular/material/menu';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatTooltipModule} from '@angular/material/tooltip';

import {AppRoutingModule} from './app-routing.module';
import {FooBarComponent} from './foo-bar/foo-bar.component';
import {HomeComponent} from './home/home.component';

import {HttpClientModule} from '@angular/common/http';
import {AuthModule, LogLevel, OidcConfigService} from 'angular-auth-oidc-client';
import {environment} from '../environments/environment';
import {UserSettingsComponent} from './user-settings/user-settings.component';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSortModule} from '@angular/material/sort';
import {HttpClientInMemoryWebApiModule} from 'angular-in-memory-web-api';
import {InMemoryContactsService} from './core/in-memory-contacts.service';
import {ContactsComponent} from './contacts/contacts.component';
import {EditContactComponent} from './edit-contact/edit-contact.component';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {ReactiveFormsModule} from '@angular/forms';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MAT_MOMENT_DATE_ADAPTER_OPTIONS, MatMomentDateModule} from '@angular/material-moment-adapter';
import { NavOrigComponent } from './nav-orig/nav-orig.component';
import { NavigationComponent } from './navigation/navigation.component';

// https://github.com/damienbod/angular-auth-oidc-client/blob/master/projects/sample-implicit-flow-silent-renew/src/app/app.module.ts
export function configureAuth(oidcConfigService: OidcConfigService) {
  return () =>
    oidcConfigService.withConfig({
      stsServer: 'https://accounts.google.com',
      redirectUrl: `${window.location.origin}/ng-starter`,
      postLogoutRedirectUri: `${window.location.origin}/ng-starter`,
      clientId: '1061811306754-2en04393j0g59iheti1h9123o96au9hu.apps.googleusercontent.com',
      scope: 'openid profile email',
      responseType: 'id_token token',
      silentRenew: false,
      silentRenewUrl: `${window.location.origin}/silent-renew.html`,
      logLevel: environment.production ? LogLevel.Warn : LogLevel.Warn,
    });
}

@NgModule({
  declarations: [
    AppComponent,
    FooBarComponent,
    HomeComponent,
    UserSettingsComponent,
    ContactsComponent,
    EditContactComponent,
    NavOrigComponent,
    NavigationComponent,
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatCardModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatMenuModule,
    MatCheckboxModule,
    MatTooltipModule,
    HttpClientModule,
    AuthModule.forRoot(),
    AppRoutingModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressBarModule,
    MatDatepickerModule,
    MatMomentDateModule,

    // The HttpClientInMemoryWebApiModule module intercepts HTTP requests
    // and returns simulated server responses.
    // Remove it when a real server is ready to receive requests.
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryContactsService, {passThruUnknownUrl: true}
    ),

    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    ReactiveFormsModule,

  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'en-AU'},
    {provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: {strict: true}},

    OidcConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configureAuth,
      deps: [OidcConfigService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
