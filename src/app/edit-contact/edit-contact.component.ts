import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ContactService} from '../core/contact.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Contact} from '../core/contact';
import {minLengthTrimmedValidator} from '../core/min-length-trimmed.directive';
import {maxLengthTrimmedValidator} from '../core/max-length-trimmed.directive';
import * as _moment from 'moment';

const moment = _moment;

@Component({
  selector: 'app-edit-contact',
  templateUrl: './edit-contact.component.html',
  styleUrls: ['./edit-contact.component.css']
})
export class EditContactComponent implements OnInit {
  private readonly routeNext = ['contacts'];

  showProgressBar = false;
  action = 'Save';
  contactForm: FormGroup;
  initialContact: Contact;
  startBirthDate: Date;
  minBirthDate: Date;
  maxBirthDate = new Date(Date.now());
  title: string;

  constructor(private activatedRoute: ActivatedRoute, private contactService: ContactService, private formBuilder: FormBuilder,
              private router: Router) {
    const currentYear = new Date().getFullYear();
    this.startBirthDate = new Date(currentYear - 30, 0, 1);
    this.minBirthDate = new Date(currentYear - 200, 0, 1);
  }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.paramMap.get('id');

    if (id) {

      if (+id > 0) {

        this.showProgressBar = true;
        this.title = 'Edit';
        this.contactService.getContact(+id).subscribe(contact => {

          if (contact) {
            this.initialContact = contact.clone();
            this.buildForm(contact);
          }
          this.showProgressBar = false;
        });

      } else {

        if (+id < 0) {

          this.showProgressBar = true;
          this.action = 'Delete';
          this.title = this.action;
          this.contactService.getContact(+id * -1).subscribe(contact => {

            if (contact) {
              this.buildForm(contact, true);
            }
            this.showProgressBar = false;
          });

        } else {
          this.title = 'Add New';
          this.buildForm({_id: null, _firstName: '', _lastName: '', _email: '', _phoneNumber: '', _birthDate: new Date()});
        }
      }
    }
  }

  private buildForm(contact: Contact | any, isDeleteAction?: boolean): void {
    this.contactForm = this.formBuilder.group({
      id: [contact._id],
      firstName: [{value: contact._firstName, disabled: isDeleteAction}, Validators.compose([
        Validators.required,
        minLengthTrimmedValidator(Contact.nameMinLength),
        maxLengthTrimmedValidator(Contact.nameMaxLength)])],
      lastName: [{value: contact._lastName, disabled: isDeleteAction}, Validators.compose([
        Validators.required,
        minLengthTrimmedValidator(Contact.nameMinLength),
        maxLengthTrimmedValidator(Contact.nameMaxLength)])],
      email: [{value: contact._email, disabled: isDeleteAction}, Validators.compose([
        Validators.required,
        minLengthTrimmedValidator(Contact.emailMinLength),
        maxLengthTrimmedValidator(Contact.emailMaxLength),
        Validators.email])],
      phoneNumber: [{value: contact._phoneNumber, disabled: isDeleteAction}, Validators.compose([
        Validators.required,
        minLengthTrimmedValidator(Contact.phoneNumberMinLength),
        maxLengthTrimmedValidator(Contact.phoneNumberMaxLength),
        Validators.pattern(Contact.phoneNumberRegExp)])],
      birthDate: [{value: moment(contact._birthDate), disabled: isDeleteAction}, Validators.required]
    });
  }

  onSubmit() {

    if ('Delete' === this.action) {
      this.showProgressBar = true;

      this.contactService.deleteContact(this.contactForm.value.id).subscribe(() => {
        this.showProgressBar = false;
        this.router.navigate(this.routeNext);
      }, () => this.showProgressBar = false);

    } else {

      if (this.contactForm.dirty) {
        const formContact = this.contactForm.value as any;
        formContact.birthDate = formContact.birthDate.toISOString(true).split('T')[0];
        const contact = Contact.fromAny(formContact);

        if (this.initialContact && this.initialContact.equals(contact)) {
          this.router.navigate(this.routeNext);
        } else {
          this.showProgressBar = true;

          if (contact._id) {

            this.contactService.updateContact(contact).subscribe(() => {
              this.showProgressBar = false;
              this.router.navigate(this.routeNext);
            }, () => this.showProgressBar = false);

          } else {

            this.contactService.addContact(contact).subscribe(() => {
              this.showProgressBar = false;
              this.router.navigate(this.routeNext);
            }, () => this.showProgressBar = false);

          }
        }
      } else {
        this.router.navigate(this.routeNext);
      }
    }
  }
}
