import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FooBarComponent} from './foo-bar/foo-bar.component';
import {HomeComponent} from './home/home.component';
import {UserSettingsComponent} from './user-settings/user-settings.component';
import {ContactsComponent} from './contacts/contacts.component';
import {UserSettingsGuard} from './core/user-settings.guard';
import {EditContactComponent} from './edit-contact/edit-contact.component';
import {NavOrigComponent} from './nav-orig/nav-orig.component';
import {NavigationComponent} from './navigation/navigation.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'foo-bar', component: FooBarComponent},
  {path: 'home-orig', component: HomeComponent},
  {path: 'nav-orig', component: NavOrigComponent},
  {path: 'home', component: NavigationComponent},
  {path: 'contacts', component: ContactsComponent},
  {path: 'contact/:id', component: EditContactComponent},
  {path: 'settings', component: UserSettingsComponent, canActivate: [UserSettingsGuard]},
  {path: '**', redirectTo: '/home'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
