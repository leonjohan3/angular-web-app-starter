import {Component, OnDestroy, OnInit} from '@angular/core';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {Observable, Subscription} from 'rxjs';
import {UserSessionService} from '../core/user-session.service';
import {Router} from '@angular/router';
import MyUtils from '../core/my-utils';

@Component({
  selector: 'app-nav-orig',
  templateUrl: './nav-orig.component.html',
  styleUrls: ['./nav-orig.component.css']
})
export class NavOrigComponent implements OnInit, OnDestroy {

  readonly drawerStateName = 'drawerOpen';
  opened: boolean;
  drawerTooltip: string;
  readonly currentYear = (new Date()).getFullYear();
  isAuthenticated$: Observable<boolean>;
  isAuthenticatedSubscription: Subscription;
  checkAuthSubscription: Subscription;
  userName: string | undefined;

  constructor(public oidcSecurityService: OidcSecurityService, private userSessionService: UserSessionService, private router: Router) {
  }

  ngOnDestroy(): void {
    this.isAuthenticatedSubscription.unsubscribe();
    this.checkAuthSubscription.unsubscribe();
  }

  ngOnInit(): void {

    if (MyUtils.hasLocalStorage()) {
      if (localStorage.getItem(this.drawerStateName) === undefined || localStorage.getItem(this.drawerStateName) === null) {
        this.opened = true;
      } else {
        this.opened = localStorage.getItem(this.drawerStateName) === 'true';
      }
    }

    this.isAuthenticated$ = this.oidcSecurityService.isAuthenticated$;

    this.checkAuthSubscription = this.oidcSecurityService.checkAuth().subscribe((isAuthenticated) => console.log('checkAuth, isAuthenticated', isAuthenticated));

    this.isAuthenticatedSubscription = this.isAuthenticated$.subscribe((isAuthenticated) => {
      console.log('isAuthenticated', isAuthenticated);

      if (isAuthenticated) {
        const payloadFromIdToken = this.oidcSecurityService.getPayloadFromIdToken();
        const attributesToIgnore = ['at_hash', 'aud', 'azp', 'iss', 'jti', 'nonce', 'sub', 'name'];

        for (const key in payloadFromIdToken) {
          if (key === 'name') {
            this.userName = payloadFromIdToken[key];
          } else {
            if (!attributesToIgnore.includes(key)) {
              this.userSessionService.getAttributes().set(key, payloadFromIdToken[key]);
            }
          }
        }
      } else {
        this.userName = undefined;
        this.userSessionService.resetAttributes();
      }
    });
  }

  setDrawerTooltipAndState(): void {
    this.drawerTooltip = (this.opened ? 'Close' : 'Open') + ' drawer';

    if (MyUtils.hasLocalStorage()) {
      localStorage.setItem(this.drawerStateName, String(this.opened));
    }
  }

  login(): void {
    this.oidcSecurityService.authorize();
  }

  logoff(): void {
    this.oidcSecurityService.logoffAndRevokeTokens().subscribe((result) => console.log('logoffAndRevokeTokens result', result));
    this.router.navigateByUrl('/');
  }
}
