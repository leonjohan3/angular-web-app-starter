import {Component, OnInit, ViewChild} from '@angular/core';
import {ContactService} from '../core/contact.service';
import {Router} from '@angular/router';
import {Contact} from '../core/contact';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator, PageEvent} from '@angular/material/paginator';
import {MatSort, Sort} from '@angular/material/sort';
import MyUtils from '../core/my-utils';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

  showProgressBar = false;

  readonly displayedColumns = ['delete', 'id', 'firstName', 'lastName', 'email', 'phoneNumber', 'birthDate'];
  readonly contactPageIndexName = 'contactPageIndexName';
  readonly contactPageSizeName = 'contactPageSizeName';
  readonly pageSizeOptions = [10, 20, 50];
  readonly contactSortId = 'contactSortId';
  readonly contactSortStart = 'contactSortStart';
  dataSource: MatTableDataSource<Contact>;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private contactService: ContactService, private router: Router) {
  }

  ngOnInit(): void {
    this.getContacts();
  }

  getContacts(): void {
    this.showProgressBar = true;

    this.contactService.getContacts().subscribe(contacts => {
      this.dataSource = new MatTableDataSource<Contact>(contacts);

      if (MyUtils.hasLocalStorage()) {
        if (localStorage.getItem(this.contactPageIndexName) !== null) {
          this.paginator.pageIndex = typeof localStorage.getItem(this.contactPageIndexName) === 'string'
            ? +localStorage.getItem(this.contactPageIndexName) : 0;
        }
        if (localStorage.getItem(this.contactPageSizeName) !== null) {
          this.paginator.pageSize = typeof localStorage.getItem(this.contactPageSizeName) === 'string'
            ? +localStorage.getItem(this.contactPageSizeName) : this.pageSizeOptions[0];
        }
        if (localStorage.getItem(this.contactSortId) !== null && localStorage.getItem(this.contactSortStart) !== null) {
          this.sort.sort({
            disableClear: false, id: localStorage.getItem(this.contactSortId),
            start: localStorage.getItem(this.contactSortStart) === 'acs' ? 'asc' : 'desc'
          });
        }
      }
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.showProgressBar = false;
    }, () => this.showProgressBar = false);
  }

  editContact(id: number): void {
    this.router.navigate(['/contact', id]);
  }

  deleteContact(id: number): void {
    this.router.navigate(['/contact', id * -1]);
  }

  addContact() {
    this.router.navigate(['/contact', 0]);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  setPaginatorIndex($event: PageEvent) {
    if (MyUtils.hasLocalStorage()) {
      localStorage.setItem(this.contactPageIndexName, String($event.pageIndex));
      localStorage.setItem(this.contactPageSizeName, String($event.pageSize));
    }
  }

  matSortChange($event: Sort) {
    if (MyUtils.hasLocalStorage()) {
      localStorage.setItem(this.contactSortId, $event.active);
      localStorage.setItem(this.contactSortStart, $event.direction);
    }
  }
}
