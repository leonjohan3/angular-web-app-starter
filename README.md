# angular-web-app-starter

## Overview
This is a [web app](<https://en.wikipedia.org/wiki/Web_application>) starter (aka a scaffold, boilerplate or seed) that is based on [Angular](<https://en.wikipedia.org/wiki/Angular_(web_framework)>) version 9.x.x and Angular 
Material version 9.x.x. It supports [authentication](<https://en.wikipedia.org/wiki/Authentication>) (using Google as an example) and different roles to demonstrate
[authorization](<https://en.wikipedia.org/wiki/Authorization>). It also supports plugging in different business [logos](<https://en.wikipedia.org/wiki/Logo>) and
[brand icons](<https://en.wikipedia.org/wiki/Icon_(computing)#Brand_icons_for_commercial_software>). It allows the customization of the [theme](<https://en.wikipedia.org/wiki/Theme_(computing)>). The application has
basic [menus](<https://en.wikipedia.org/wiki/Menu_(computing)>), a basic [input form](<https://en.wikipedia.org/wiki/Form_(HTML)>) and a basic [dashboard](<https://en.wikipedia.org/wiki/Dashboard_(business)>). To keep the
application self-contained, the backend functionality has been mocked by using [Web localstorage](<https://en.wikipedia.org/wiki/Web_storage#Local_and_session_storage>) 
and [Angular in-memory-web-api](<https://github.com/angular/in-memory-web-api>).

The menus, form and dashboard can easily be changed and replaced by following the instructions in this README. 

Deployed application: https://leonjohan3.bitbucket.io/ng-starter

## Installing, running and building
To deploy the application locally, clone the repository, and then in the project folder, run the commands below:

    npm install
    npm run eslint && ng l --typeCheck=true
    ng serve

## Changing the theme
* [To change the color theme of the application](<https://bitbucket.org/leonjohan3/angular-web-app-starter/src/master/>) [todo]
* [Adjusting the colors of UI elements](<https://bitbucket.org/leonjohan3/angular-web-app-starter/src/master/>) [todo]

## Changing the authentication provider
* Edit the [config-oidc.js](<https://bitbucket.org/leonjohan3/angular-web-app-starter/src/master/>) [todo - fix link] file in the config folder.
* The OAuth clientId has been externalised and should be added to the environment variables, see [todo](<https://bitbucket.org/leonjohan3/angular-web-app-starter/src/master/>)

## Todo
1.  Externalize the Google oidc client id.
2.  Create a user profile screen showing first name, surname, email, email verified and token expire time.
3.  Enable code coverage (see https://angular.io/guide/testing). [not-do]

## Command line
* Update the project to the latest patch version `ng update @angular/cli@^9 @angular/core@^9`
* Install the latest patch version of the cli `npm uninstall -g angular-cli && npm install -g @angular/cli@^9`

## Resources
* [README.md Markup Guide](<https://spec.commonmark.org/0.29/>)
* [README.md Markup Tables](<https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#tables/>)
* [TypeScript](<https://www.typescriptlang.org/docs/handbook/basic-types.html>)
* [TypeScript Classes](<https://www.typescriptlang.org/docs/handbook/classes.html>)
* [TypeScript Tutorial](<https://www.typescripttutorial.net>)
* [JavaScript](<https://javascript.info>)
* [Angular](<https://angular.io/docs>)
* [Angular Testing](<https://angular.io/guide/testing>)
* [Angular Material](<https://material.angular.io>)
* [Material Design Specification](<https://material.io/design/introduction>)
* [Material Icons](<https://material.io/resources/icons/?style=baseline>)
* [Angular Flex-Layout](<https://github.com/angular/flex-layout/>)
* [OpenID Connect](<https://en.wikipedia.org/wiki/OpenID_Connect>)
* [oidc-client](<https://github.com/IdentityModel/oidc-client-js/wiki>)
* [Google OAuth Documentation](<https://developers.google.com/identity/protocols/OAuth2UserAgent>)
* [Angular oidc client](<https://www.npmjs.com/package/angular-auth-oidc-client>)
* [Angular oidc client (Github)](<https://github.com/damienbod/angular-auth-oidc-client>)
* [OpenID Provider (for local testing) ](<https://www.ory.sh/hydra/docs/5min-tutorial/>)
* [In-memory web api](<https://github.com/angular/in-memory-web-api>)
* [Moment](<https://momentjs.com/docs/>)
* [Proxying to a backend server](<https://angular.io/guide/build#proxying-to-a-backend-server>)
* [Bootstrap](<https://getbootstrap.com/docs/4.5/getting-started/introduction/>)
* [Angular Bootstrap](<https://ng-bootstrap.github.io/#/home>)
* [Angular Augury](<https://augury.rangle.io/pages/guides/index.html>)
* https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout
* https://developer.mozilla.org/en-US/docs/Web/CSS/Reference
* https://cssgradient.io/
* http://www.css3.info/modules/
* https://medium.com/letsboot/quick-start-with-angular-material-and-flex-layout-1b065aa1476c

## Other
* https://www.djamware.com/post/5d58b409bcc156d4a8a3df8f/angular-8-tutorial-routing-navigation-example
* https://stackoverflow.com/questions/18915550/fix-footer-to-bottom-of-page
* https://demo.psma.com.au/predictive-address-verification
